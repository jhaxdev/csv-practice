package reader

import (
	"os"

	"github.com/gocarina/gocsv"
)

// CSV Header:
// Date,Open,High,Low,Close,Volume

type Price struct {
	Date   string
	Open   float32
	High   float32
	Low    float32
	Close  float32
	Volume string
}

// Read the CSV file and return a slice containing Price structs
func ReadCSV() []*Price {
	datadir := "data/"
	filename := "XNYS_July-Aug.csv"
	file, _ := os.Open(datadir + filename) // get a handle for the csv file
	defer file.Close()                     // make sure file handle closes after function terminates

	prices := []*Price{}                                       // initialize slice of struct price
	if err := gocsv.UnmarshalFile(file, &prices); err != nil { // Load clients from file
		panic(err)
	}

	return prices

}
