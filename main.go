package main

import (
	"fmt"

	"gitlab.com/jhax/csv-practice/reader"
)

func getAverage(prices []float32) float32 {
	numPrice := float32(len(prices))
	var priceSum float32
	for _, v := range prices {
		priceSum = priceSum + v
	}
	avg := priceSum / numPrice

	return avg
}

func main() {
	xnysPrices := reader.ReadCSV()
	nPrices := len(xnysPrices)

	startDate := xnysPrices[nPrices-1].Date
	endDate := xnysPrices[0].Date

	var closePrices []float32
	for _, v := range xnysPrices {
		closePrices = append(closePrices, v.Close) // create slice of just the closing prices
	}
	myAvg := getAverage(closePrices) // get average of the closing prices

	fmt.Printf("Closing average from %s-%s: %f\n", startDate, endDate, myAvg)

}
